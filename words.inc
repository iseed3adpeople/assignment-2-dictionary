%include 'colon.inc'

section .rodata
global dict_ptr

colon "key3", third_key
db "test test test", 0

colon "key2", second_key
db "test test", 0

colon "key1", dict_ptr
db "test", 0
