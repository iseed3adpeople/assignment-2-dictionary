compile_flags = nasm -f elf64 -g

main.o: main.asm words.inc
	compile_flags main.asm -o main.o

main: main.o lib.o dict.o
	ld -o main main.o lib.o dict.o

lib.o: lib.inc lib.asm
	compile_flags lib.asm -o lib.o

dict.o: dict.inc dict.asm
	compile_flags dict.asm -o dict.o

.PHONY: run test clean
run: main
	./main

test: main
	python3 -u test.py
	
clean:
	rm -f *.o main
